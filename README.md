# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Summary: This application connects to http://agl-developer-test.azurewebsites.net/ and processes the response to show all cats based on owner gender.
* Version 1.0.0.0

### How do I get set up? ###

* The application contains the following components
	- AGL.CodingChallenge.DataAccess 
		-> Contains IService.cs interface
		-> Contains Service.cs class which implements the IService interface - used for connecting to agl API, retreiving and deserialising the data
		-> The application is configured to connect to the AGL Api http://agl-developer-test.azurewebsites.net/, deserialises the JSON response and get the test data.
		-> Newtonsoft has been used as the prefered JSON framework for serialising and deserialising JSON. This has been added as a nuget package. The references will be resolved when the solution is built.
	- AGL.CodingChallenge.Models
		-> Contains internal and external model classes and enums used by the application
	- AGL.CodingChallenge.Business
		-> Contains Processor.cs class which impliments the IProcessor interface. The public method GetPetNamesByOwnerGender 
		-> The processor class contains GetPetNamesByOwnerGender method which takes in the PetType as a parameter and returns the external display object containing the pet names segregated by the gender of the owner
	- AGL.CodingChallenge.Tests
		-> Contains application test cases.
		-> MSTest has been used for test cases How to run tests



### Contribution guidelines ###

* Test cases written to test for returned cat count, cat names and verified that there are no other pet types returned
* Removed dead code and unused usings
* Have not used Unity or Moq in the solution to keep it simple. I would probably use it in a larger solution.
* Logging is done using System.Diagnostics using a writer listener configured in the App.config. 

### Who do I talk to? ###

* email : subinninan@gmail.com
