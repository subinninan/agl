﻿using AGL.CodingChallenge.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Reflection;
using Newtonsoft.Json;
using System.Diagnostics;

namespace AGL.CodingChallenge.DataAccess
{
    public sealed class Service : IService
    {
        private string Url;

        public Service(string url)
        {
            this.Url = url;
        }

        private HttpClient GetHttpClient()
        {
            try
            {
                HttpClient httpClient = (HttpClient)new HttpClient();
                return httpClient;

            }
            catch (Exception ex)
            {
                var error = string.Format("Method: GetHttpClient(). Exception: {0}", ex.Message);
                Trace.TraceError(error);
                throw new Exception(error);
            }
        }

        public List<Person> GetData()
        {
            try
            {
                string responseData;
                List<Person> response = null;
                using (var client = GetHttpClient())
                {
                    responseData = client.GetStringAsync(Url).Result;
                }

                if (!string.IsNullOrWhiteSpace(responseData))
                {
                    response = JsonConvert.DeserializeObject<List<Person>>(responseData);
                }
                return response;
            }
            catch(Exception ex)
            {
                var error = string.Format("Method: GetData(). Exception: {0}", ex.Message);
                Trace.TraceError(error);
                throw new Exception(error);
            }

            
        }

    }
}
