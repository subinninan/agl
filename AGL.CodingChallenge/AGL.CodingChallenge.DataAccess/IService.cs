﻿using AGL.CodingChallenge.Models;
using System.Collections.Generic;

namespace AGL.CodingChallenge.DataAccess
{
    public interface IService
    {
        List<Person> GetData();

    }
}
