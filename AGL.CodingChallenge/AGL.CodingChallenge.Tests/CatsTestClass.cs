﻿using System.Net.Http;
using AGL.CodingChallenge.DataAccess;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections;
using System.Linq;
using AGL.CodingChallenge.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using AGL.CodingChallenge.Business;
using AGL.CodingChallenge.Tests.MockData;

namespace AGL.CodingChallenge.Tests
{
    [TestClass]
    public class DataAccessTestClass
    {
        IService service;
        IProcessor processor;

        [TestInitialize]
        public void Initialize()
        {
            //inject data service to processor class
            service = new MockService();
            processor = new Processor(service);
        }
      
        [TestMethod]
        public void Get_Cat_Names_Based_On_Gender_Of_Owner_Check_Lists_Not_Null_Test()
        {
            var petNameByOwnerGender = processor.GetPetNamesByOwnerGender(PetType.Cat);
            //check lists are not null
            Assert.IsNotNull(petNameByOwnerGender);
            Assert.IsNotNull(petNameByOwnerGender.Male);
            Assert.IsNotNull(petNameByOwnerGender.Female);
        }


        [TestMethod]
        public void Check_Cat_Count_Test()
        {

            var petNameByOwnerGender = processor.GetPetNamesByOwnerGender(PetType.Cat);
            Assert.IsNotNull(petNameByOwnerGender);
            Assert.IsNotNull(petNameByOwnerGender.Male);
            Assert.IsNotNull(petNameByOwnerGender.Female);

            //Checking list contains 4 pets for Male owners and 3 pets for Female Owners
            Assert.AreEqual(petNameByOwnerGender.Male.Count, 4);
            Assert.AreEqual(petNameByOwnerGender.Female.Count, 3 );
        }

        [TestMethod]
        public void Check_All_Cats_Returned_Test()
        {
            var petNameByOwnerGender = processor.GetPetNamesByOwnerGender(PetType.Cat);

            //Check all cats are accounted for
            Assert.AreEqual(1, (petNameByOwnerGender.Male.Where(s => s.Name == "Garfield")).ToList().Count);
            Assert.AreEqual(1, (petNameByOwnerGender.Male.Where(s => s.Name == "Tom")).ToList().Count);
            Assert.AreEqual(1, (petNameByOwnerGender.Male.Where(s => s.Name == "Max")).ToList().Count);
            Assert.AreEqual(1, (petNameByOwnerGender.Male.Where(s => s.Name == "Jim")).ToList().Count);

            Assert.AreEqual(1, (petNameByOwnerGender.Female.Where(s => s.Name == "Garfield")).ToList().Count);
            Assert.AreEqual(1, (petNameByOwnerGender.Female.Where(s => s.Name == "Tabby")).ToList().Count);
            Assert.AreEqual(1, (petNameByOwnerGender.Female.Where(s => s.Name == "Simba")).ToList().Count);
        }

        [TestMethod]
        public void Check_All_Sorted_Cats_Returned_Test()
        {
            var petNameByOwnerGender = processor.GetPetNamesByOwnerGender(PetType.Cat);

            //Check all cats are accounted for
            Assert.AreEqual("Garfield", petNameByOwnerGender.Male[0].Name);
            Assert.AreEqual("Jim", petNameByOwnerGender.Male[1].Name);
            Assert.AreEqual("Max", petNameByOwnerGender.Male[2].Name);
            Assert.AreEqual("Tom", petNameByOwnerGender.Male[3].Name);

            Assert.AreEqual("Garfield", petNameByOwnerGender.Female[0].Name);
            Assert.AreEqual("Simba", petNameByOwnerGender.Female[1].Name);
            Assert.AreEqual("Tabby", petNameByOwnerGender.Female[2].Name);
        }

        [TestMethod]
        public void Check_Only_Cats_Returned_Test()
        {
            var petNameByOwnerGender = processor.GetPetNamesByOwnerGender(PetType.Cat);

            //Check only cats are returned
            Assert.AreEqual(0, (petNameByOwnerGender.Male.Where(s => s.Type != PetType.Cat)).ToList().Count);
            Assert.AreEqual(0, (petNameByOwnerGender.Female.Where(s => s.Type != PetType.Cat)).ToList().Count);
            
        }


    }
}
