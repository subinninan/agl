﻿using AGL.CodingChallenge.Models;

namespace AGL.CodingChallenge.Business
{
    public interface IProcessor
    {
        PetNamesByOwnerGender GetPetNamesByOwnerGender(PetType petType);
    }
}
