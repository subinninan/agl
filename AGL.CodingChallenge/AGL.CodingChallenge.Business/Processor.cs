﻿using System.Collections.Generic;
using System.Configuration;
using AGL.CodingChallenge.DataAccess;
using AGL.CodingChallenge.Models;
using System.Linq;
using System;
using System.Diagnostics;

namespace AGL.CodingChallenge.Business
{
    public class Processor : IProcessor
    {
        IService Service;
        
        public Processor(IService service)
        {
            if(service != null)
                this.Service = service;
            else
            {
                var url = ConfigurationManager.AppSettings.Get("ServiceUrl");
                this.Service = new Service(url);
            }
        }

        public PetNamesByOwnerGender GetPetNamesByOwnerGender(PetType petType)
        {
            try
            {
                //Get data to act on
                var response = Service.GetData();

                var petsByOwnerGender = new PetNamesByOwnerGender();

                //get pets for male owners
                var petsForMaleOwners = GetPetsByTypeAndOwnerGender(response, Gender.Male, petType);

                //add the pets to display model 
                if (petsForMaleOwners != null && petsForMaleOwners.Count > 0)
                    petsByOwnerGender.Male.AddRange(petsForMaleOwners);

                //get pets for female owners
                var petsForFemaleOwners = GetPetsByTypeAndOwnerGender(response, Gender.Female, petType);

                //add the pets to display model
                if (petsForFemaleOwners != null && petsForFemaleOwners.Count > 0)
                    petsByOwnerGender.Female.AddRange(petsForFemaleOwners);

                return petsByOwnerGender;
            }
            catch(Exception ex)
            {
                var error = string.Format("Method: GetPetNamesByOwnerGender(PetType petType) Exception: {0}", ex.Message);
                Trace.TraceError(error);
                throw new Exception(error);
            }
        }

        private List<Pet> GetPetsByTypeAndOwnerGender(List<Person> Owners, Gender ownerGender, PetType petType)
        {
            try
            {
                //retun data filtered by owner gender and pet type.
                return (from persons in Owners
                        where persons.Gender == ownerGender && persons.Pets != null
                        from pets in persons.Pets
                        where pets.Type == petType
                        select pets).OrderBy(p => p.Name).ToList();
            }
            catch(Exception ex)
            {
                var error = string.Format("Method: GetPetsByTypeAndOwnerGender(List<Person> Owners, Gender ownerGender, PetType petType) Exception: {0}", ex.Message);
                Trace.TraceError(error);
                throw new Exception(error);
            }
        }
    }
}
