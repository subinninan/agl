﻿using AGL.CodingChallenge.Models;
using System.Configuration;
using AGL.CodingChallenge.DataAccess;
using AGL.CodingChallenge.Business;
using System;
using System.Linq;
using System.Diagnostics;

namespace AGL.CodingChallenge
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var url = ConfigurationManager.AppSettings.Get("ServiceUrl");

                IService service = new Service(url);
                IProcessor processor = new Processor(service);

                var catsByOwnerGender = processor.GetPetNamesByOwnerGender(PetType.Cat);

                //output all cats for male owners
                Console.OutputEncoding = System.Text.Encoding.UTF8;
                Console.WriteLine("Male" + Environment.NewLine);
                Console.Write("  •  ");
                Console.WriteLine(String.Join(Environment.NewLine + "  •  ", catsByOwnerGender.Male.Select(s => s.Name)));

                //output all cats for female owners
                Console.WriteLine(Environment.NewLine);
                Console.WriteLine("Female" + Environment.NewLine);
                Console.Write("  •  ");
                Console.WriteLine(String.Join(Environment.NewLine + "  •  ", catsByOwnerGender.Female.Select(s => s.Name)));
                Console.ReadKey();
            }
            catch(Exception ex)
            {
                var error = string.Format("Method: GetPetNamesByOwnerGender(PetType petType) Exception: {0}", ex.Message);
                Trace.TraceError(error);
                //Display non descriptive message to user.
                Console.Write("The application has run into and error. Please see logs for mode details.");
            }

            


        }
    }
}
