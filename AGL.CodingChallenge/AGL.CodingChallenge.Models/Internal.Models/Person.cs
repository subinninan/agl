﻿using System.Collections.Generic;
using Newtonsoft.Json;
namespace AGL.CodingChallenge.Models
{
    public partial class Person
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("gender")]
        public Gender Gender { get; set; }

        [JsonProperty("age")]
        public long Age { get; set; }

        [JsonProperty("pets")]
        public List<Pet> Pets { get; set; }
    }
}
