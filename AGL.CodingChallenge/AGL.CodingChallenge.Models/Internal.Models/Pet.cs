﻿using Newtonsoft.Json;

namespace AGL.CodingChallenge.Models
{
    public partial class Pet
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("type")]
        public PetType Type { get; set; }
    }
}
