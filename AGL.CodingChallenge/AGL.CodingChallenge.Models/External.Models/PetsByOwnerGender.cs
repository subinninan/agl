﻿using System.Collections.Generic;
namespace AGL.CodingChallenge.Models
{
    public partial class PetNamesByOwnerGender
    {
        public PetNamesByOwnerGender()
        {
            this.Male = new List<Pet>();
            this.Female = new List<Pet>();
        }
        public List<Pet> Male { get; set; }
        public List<Pet> Female { get; set; }
    }
}
